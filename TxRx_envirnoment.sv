class TxRx_environment extends ovm_env;
TxRx_agent agent;
scoreboard sb;
//registering
`ovm_component_utils(TxRx_environment)

//constructor
function new(string name = " ", ovm_component parent = null);
super.new(name,parent);
endfunction: new

function void build();
super.build();
agent = TxRx_agent::type_id::create("agent",this);
sb = scoreboard::type_id::create("sb",this);
endfunction : build

function void connect();
agent.agenttb.connect(sb.sb_tb);
agent.agentdut.connect(sb.sb_dut);
endfunction : connect 

endclass : TxRx_environment