class TxRx_monitor_dut extends ovm_monitor;
TxRx_sequence_item seq_item;
virtual TxRx_intf intf;
Wrapper wrapper;
ovm_object dummy;
int state;
int counter;
ovm_analysis_port #(TxRx_sequence_item) mondut2sb;
//registering
`ovm_component_utils(TxRx_monitor_dut)
//constructor
function new(string name = " ", ovm_component parent = null);
super.new(name,parent);
endfunction: new

virtual function void build();
//casting
if(!get_config_object("configuration",dummy,0))
ovm_report_info(get_type_name(),"dummy is not assigned",OVM_LOG);
else
ovm_report_info(get_type_name(),"dummy is assigned",OVM_LOG); 


if($cast(wrapper,dummy))
begin
ovm_report_info(get_type_name(),"object configured properly",OVM_LOG);
intf = wrapper.intf;
mondut2sb = new("mondut2sb",this);
end
else
ovm_report_info(get_type_name(),"dummy is really dummy",OVM_LOG);

endfunction : build

virtual task run();
seq_item = TxRx_sequence_item::type_id::create("seq_item");

forever
begin
@(posedge intf.clk)
 if (intf.rx_data != 0)
 begin 
  seq_item.rx_data = intf.rx_data;
  mondut2sb.write(seq_item); 
 end

end
endtask : run

endclass : TxRx_monitor_dut