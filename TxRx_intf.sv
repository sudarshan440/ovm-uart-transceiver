interface TxRx_intf;
logic [7:0] tx_data;
logic transmit;
logic rst;
logic clk;
logic [7:0] rx_data;
endinterface