class TxRx_sequence_item extends ovm_sequence_item;
// variable declaration 
bit transmit;
rand bit [7:0] tx_data;
bit [7:0] rx_data;

//bit txd;
// registering with factory
`ovm_object_utils_begin(TxRx_sequence_item)
`ovm_field_int(tx_data,OVM_ALL_ON);
`ovm_field_int(rx_data,OVM_ALL_ON);
`ovm_field_int(transmit,OVM_ALL_ON);
//`ovm_field_int(txd,OVM_ALL_ON);
`ovm_object_utils_end
//constructor
function new(string name = " " );
super.new(name);
endfunction: new

//declaration of constraints

endclass : TxRx_sequence_item