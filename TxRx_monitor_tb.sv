class TxRx_monitor_tb extends ovm_monitor;
TxRx_sequence_item seq_item;
virtual TxRx_intf intf;
Wrapper wrapper;
ovm_object dummy;

ovm_analysis_port #(TxRx_sequence_item) montb2sb;
//registering
`ovm_component_utils(TxRx_monitor_tb)
//coverage collector
byte tx_data;

covergroup cg;

txdata : coverpoint tx_data;

endgroup : cg 


//constructor
function new(string name = " ", ovm_component parent = null);
super.new(name,parent);
 cg = new;
endfunction: new


virtual function void build();
//casting
if(!get_config_object("configuration",dummy,0))
ovm_report_info(get_type_name(),"dummy is not assigned",OVM_LOG);
else
ovm_report_info(get_type_name(),"dummy is assigned",OVM_LOG); 

if($cast(wrapper,dummy))
  begin
ovm_report_info(get_type_name(),"object configured properly",OVM_LOG);
intf = wrapper.intf;
montb2sb = new("montb2sb",this);
end
else
ovm_report_info(get_type_name(),"dummy is really dummy",OVM_LOG);

endfunction : build

virtual task run();
seq_item = TxRx_sequence_item::type_id::create("seq_item");

forever
begin
  @(posedge intf.clk)
  begin
     if ( intf.transmit == 1'b1)
       begin

         seq_item.tx_data = intf.tx_data;
		 tx_data = intf.tx_data;
          cg.sample();
		  
         //model(); 
         montb2sb.write(seq_item);
       end
   end //clock
end //forever loop
endtask : run

function model();
$display("dummy model ");
endfunction : model
endclass : TxRx_monitor_tb