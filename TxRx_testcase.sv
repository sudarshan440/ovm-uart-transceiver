class TxRx_testcase extends ovm_test;
TxRx_sequence seq;
TxRx_sequencer sequencer;
TxRx_environment env;
//registering
`ovm_component_utils(TxRx_testcase)

//constructor
function new(string name = " ", ovm_component parent= null);
super.new(name,parent);

endfunction : new

function void build();
  super.build();
  env = TxRx_environment::type_id::create("env",this);
endfunction : build


task run();
$cast(sequencer,env.agent.sequencer);
sequencer.count = 0;
 seq = TxRx_sequence::type_id::create("seq"); 
seq.start(sequencer);
endtask 

endclass : TxRx_testcase