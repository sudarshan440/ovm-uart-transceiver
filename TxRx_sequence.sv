class TxRx_sequence extends ovm_sequence#(TxRx_sequence_item);
TxRx_sequence_item seq_item;

//registering 
`ovm_object_utils(TxRx_sequence)

//constructor

function new(string name = " ");
super.new(name);
endfunction 

// body

virtual task body();
repeat(10)
begin
seq_item = TxRx_sequence_item::type_id::create("seq_item");
wait_for_grant();
//seq_item.randomize() with {tx_data == 8'b01010101;};
seq_item.randomize();
//seq_item.randomize() with {transmit == 1'b1;};
//assert(seq_item.randomize() );
send_request(seq_item);
wait_for_item_done();
end
endtask : body
endclass : TxRx_sequence