
// Code your design here
module top;

import TxRxPkg::*;
  import TxRxTestPkg::*;
TxRx_intf intf();

initial
begin
intf.clk = 0;
end
//wire validation;

always #10 intf.clk = ~intf.clk;

txrxtop topdut (.clk(intf.clk),.reset(intf.rst),.transmit(intf.transmit),.tx_data(intf.tx_data),.rx_data(intf.rx_data));
  
  //transmitter(clk, reset, transmit, data, Txd );
  //transmitter tx(clk, reset, transmit, data, Txd );
  //receiver rx (clk, reset,rx_data,rxd);

  //assign rxd = Txd;
  
  //assign validation = (rx_data == data)?1:0;

initial
begin
Wrapper wrapper = new("Wrapper");
 wrapper.setVintf(intf);

set_config_object("*","configuration",wrapper,0);
run_test("TxRx_testcase");
#1000;
$finish;
end

  
endmodule 