class Wrapper extends ovm_object;
virtual TxRx_intf intf;

`ovm_object_utils(Wrapper)

function new(string name = " " );
super.new(name);
endfunction : new

function setVintf(virtual TxRx_intf intf);
 this.intf=intf;
endfunction:setVintf 

endclass : Wrapper 