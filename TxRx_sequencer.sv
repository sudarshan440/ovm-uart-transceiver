class TxRx_sequencer extends ovm_sequencer#(TxRx_sequence_item);

//registering
`ovm_component_utils(TxRx_sequencer)

//constructor
function new(string name = " ", ovm_component parent = null);
super.new(name,parent);
endfunction : new

endclass : TxRx_sequencer