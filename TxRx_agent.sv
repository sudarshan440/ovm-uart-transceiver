class TxRx_agent extends ovm_agent;
TxRx_monitor_tb  monitortb;
TxRx_monitor_dut  monitordut;
TxRx_driver driver;
TxRx_sequencer sequencer;

ovm_analysis_port #(TxRx_sequence_item) agenttb;
ovm_analysis_port #(TxRx_sequence_item) agentdut;

//registering
`ovm_component_utils(TxRx_agent)

//constructor
function new (string name = " ", ovm_component parent = null);
super.new(name,parent);
endfunction : new

function void build();
super.build();
agenttb = new("agenttb",this);
agentdut = new("agentdut",this);

sequencer = TxRx_sequencer::type_id::create("sequencer",this);
driver = TxRx_driver::type_id::create("driver",this);
monitortb = TxRx_monitor_tb::type_id::create("monitortb",this);
monitordut = TxRx_monitor_dut::type_id::create("monitordut",this);
endfunction : build

function void connect();
driver.seq_item_port.connect(sequencer.seq_item_export);
monitortb.montb2sb.connect(agenttb);
monitordut.mondut2sb.connect(agentdut);

endfunction : connect


endclass : TxRx_agent