// Code your design here
module txrxtop(clk,reset,transmit,tx_data,rx_data);
  
  input clk;
  input reset;
  input transmit;
  input wire [7:0] tx_data;
  output wire [7:0] rx_data;
  
  wire validation;
  wire rxd;
  wire Txd;
  //transmitter(clk, reset, transmit, data, Txd );
  transmitter tx(clk, reset, transmit, tx_data, Txd );
  receiver rx (clk, reset,rx_data,rxd);

  assign rxd = Txd;
  //assign tx_clk = clk;
  //assign rx_clk = clk;
  //assign tx_reset = reset;
  //assign rx_reset  = reset;
  //assign tx_data = data;
  
  assign validation = (rx_data == tx_data)?1:0;
  
endmodule 